package fr.dwaps.dev;

import java.util.ArrayList;
import java.util.List;

import fr.dwaps.model.Address;
import fr.dwaps.model.Contact;
import fr.dwaps.model.Repertoire;
import fr.dwaps.model.Type;

public final class Fixtures {
	private Fixtures() {}
	
	public static List<Repertoire> getRepertoires() {
		List<Repertoire> repertoires = List.of(
			new Repertoire("Mes contacts", getContacts(Type.CONTACTS), Type.CONTACTS),
			new Repertoire("Mes amis", getContacts(Type.FRIENDS), Type.FRIENDS),
			new Repertoire("Ma famille", getContacts(Type.FAMILY), Type.FAMILY),
			new Repertoire("Mes coll�gues", getContacts(Type.WORKERS), Type.WORKERS),
			new Repertoire("Les perdants", getContacts(Type.LOOSERS), Type.LOOSERS)
		);
		
		return repertoires;
	}
	
	private static List<Contact> getContacts(Type type) {
		List<Contact> contacts = new ArrayList<>();
		
		Address address = new Address("32 avenue de la Libert�", 23564, "Tartampion");
		
		switch (type) {
			case CONTACTS:
				contacts.add(new Contact("St�phane", "Durand", "054678595", address));
				contacts.add(new Contact("Emilie", "Kovalski", "054678595", address));
				contacts.add(new Contact("Simon", "Kaicou", "0654878787", address));
				contacts.add(new Contact("Audrey", "Salomon", "054678595", address));
				contacts.add(new Contact("Fernand", "Tafouk", "0213457855", address));
				break;
			case FAMILY:
				contacts.add(new Contact("Coco", "Pulain", "054678595", address));
				contacts.add(new Contact("Jaco", "Pulain", "054678595", address));
				contacts.add(new Contact("Pedro", "Pulain", "054678595", address));
				break;
			case WORKERS:
				contacts.add(new Contact("Fernand", "Castari", "054678595", address));
				contacts.add(new Contact("St�phanie", "Kaloust", "054678595", address));
				break;
			case FRIENDS:
				contacts.add(new Contact("Fernand", "Castari", "054678595", address));
				contacts.add(new Contact("St�phanie", "Kaloust", "054678595", address));
				break;
			case LOOSERS:
				contacts.add(new Contact("Fernand", "Castari", "054678595", address));
				contacts.add(new Contact("Fernand", "Castari", "054678595", address));
				contacts.add(new Contact("St�phanie", "Kaloust", "054678595", address));
				contacts.add(new Contact("St�phanie", "Kaloust", "054678595", address));
				contacts.add(new Contact("St�phanie", "Kaloust", "054678595", address));
		}
		
		
		return contacts;
	}
}
