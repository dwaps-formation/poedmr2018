package fr.dwaps.model;

import java.io.Serializable;

public class Contact implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String firstname;
	private String lastname;
	private String phone;
	private Address address;
	
	public Contact() {}
	public Contact(String firstname, String lastname, String phone) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.phone = phone;
	}
	public Contact(String firstname, String lastname, String phone, Address address) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.phone = phone;
		this.address = address;
	}
	public Contact(int id, String firstname, String lastname, String phone) {
		this(firstname, lastname, phone);
		this.id = id;
	}
	public Contact(int id, String firstname, String lastname, String phone, Address address) {
		this(id, firstname, lastname, phone);
		this.address = address;
	}
	
	public void setId(int id) { this.id = id; }
	public void setFirstname(String firstname) { this.firstname = firstname; }
	public void setLastname(String lastname) { this.lastname = lastname; }
	public void setPhone(String phone) { this.phone = phone; }
	public void setAddress(Address address) { this.address = address; }
	
	public int getId() { return id; }
	public String getFirstname() { return firstname; }
	public String getLastname() { return lastname; }
	public String getPhone() { return phone; }
	public Address getAddress() { return address; }
	
	@Override
	public String toString() {
		return "* " + lastname.toUpperCase() + " " + firstname
				+ "\n\t" + phone
				+ "\n\t" + address.getStreet() + ", " + address.getZipcode() + " " + address.getCity().toUpperCase() + "\n";
	}
}
