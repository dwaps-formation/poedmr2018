package fr.dwaps.model;

import java.io.Serializable;
import java.util.List;

public class Repertoire implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private List<Contact> contacts;
	private Type type;
	
	public Repertoire() {}
	public Repertoire(String name, List<Contact> contacts, Type type) {
		super();
		this.name = name;
		this.contacts = contacts;
		this.type = type;
	}
	public Repertoire(int id, String name, List<Contact> contacts, Type type) {
		this(name, contacts, type);
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<Contact> getContacts() {
		return contacts;
	}
	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}
	
	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}
		
	@Override
	public String toString() {
		return "Repertoire [id=" + id + ", name=" + name + ", contacts=" + contacts + ", type=" + type + "]";
	}
}
