package fr.dwaps.model;

import java.io.Serializable;

public final class Address implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String street;
	private int zipcode;
	private String city;
	
	public Address() {}
	public Address(String street, int zipcode, String city) {
		this.street = street;
		this.zipcode = zipcode;
		this.city = city;
	}
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public int getZipcode() {
		return zipcode;
	}
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Override
	public String toString() {
		return "Address [street=" + street + ", zipcode=" + zipcode + ", city=" + city + "]";
	}
}
