package fr.dwaps.controller;

import java.util.List;
import java.util.Scanner;

import fr.dwaps.dev.Fixtures;
import fr.dwaps.model.Repertoire;
import fr.dwaps.view.Displayable;
import fr.dwaps.view.ScreenDisplayer;

public class AppCtrl {
	public void start(Scanner sc) {
		// Aller chercher les donn�es
		List<Repertoire> repertoires = Fixtures.getRepertoires();
		
		// Les passer � la vue
		Displayable displayer = new ScreenDisplayer(repertoires);
		displayer.homeScreen(sc);
	}
}
