package fr.dwaps.controller;

import java.util.Scanner;

public class FrontCtrl {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		new AppCtrl().start(sc);
		
		sc.close();
	}
}