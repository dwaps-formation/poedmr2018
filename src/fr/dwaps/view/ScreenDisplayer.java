package fr.dwaps.view;

import static fr.dwaps.util.Constants.END_OF_PROGRAM;
import static fr.dwaps.util.Constants.WELCOME;
import static fr.dwaps.util.Constants.WHAT_REPERTOIRE;

import java.util.List;
import java.util.Scanner;

import fr.dwaps.model.Contact;
import fr.dwaps.model.Repertoire;
import fr.dwaps.model.Type;

public final class ScreenDisplayer implements Displayable {
	private StringBuilder sb;
	private List<Repertoire> repertoires;
	
	public ScreenDisplayer(List<Repertoire> repertoires) {
		this.repertoires = repertoires;
	}

	@Override
	public void homeScreen(Scanner sc) {
		sb = new StringBuilder();
		
		// Titre accueil
		decorateString(WELCOME);
		sb.append(WELCOME);
		decorateString(WELCOME);
		
		// Question � l'utilisateur
		sb.append(WHAT_REPERTOIRE);
		for (Type t : Type.values()) {
			sb.append(t + " ? ");
		}
		System.out.println(sb + "\n");
		
		// Traitement de la r�ponse utilisateur
		buildResponse(sc);
	}
	
	private void buildResponse(Scanner sc) {
		String response = sc.nextLine().toUpperCase();
		
		try {
			Type.valueOf(response);
		}
		catch (IllegalArgumentException e) {
			homeScreen(sc);
		}
		
		for (Repertoire repertoire : repertoires) {
			if (repertoire.getType().name().equals(response)) {
				repScreen(repertoire);
			}
		}
	}
	
	private void decorateString(String str) {
		for (char c : str.toCharArray()) {
			sb.append("-");
		}
		sb.append("\n");
	}

	@Override
	public void repScreen(Repertoire repertoire) {
		sb = new StringBuilder();
		String repTitle =  " " + repertoire.getName() + "\n";
		
		sb.append("\n");
		decorateString(repTitle);
		sb.append(repTitle);
		decorateString(repTitle);
		
		System.out.println(sb);
		
		for (Contact c : repertoire.getContacts()) {
			System.out.println(c);
		}
	}

	@Override
	public void formScreen(Scanner sc) {
	}

	@Override
	public void endScreen() {
		decorateString(END_OF_PROGRAM);
		System.out.println(END_OF_PROGRAM);
		decorateString(END_OF_PROGRAM);
	}
}
