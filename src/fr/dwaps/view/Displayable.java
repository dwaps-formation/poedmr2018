package fr.dwaps.view;

import java.util.Scanner;

import fr.dwaps.model.Repertoire;

public interface Displayable {
	void homeScreen(Scanner sc);
	void repScreen(Repertoire repertoire);
	void formScreen(Scanner sc);
	void endScreen();
}
